toml11 (4.4.0-1) unstable; urgency=medium

  * New upstream version
  * d/patches: Refresh patches.

 -- Lance Lin <lq27267@gmail.com>  Thu, 20 Feb 2025 20:17:25 +0700

toml11 (4.3.0-1) unstable; urgency=medium

  * New upstream version
  * d/patches: Refresh patches.
  * d/copyright: Update copyright year for debian files.

 -- Lance Lin <lq27267@gmail.com>  Fri, 17 Jan 2025 21:45:21 +0700

toml11 (4.2.0-1) unstable; urgency=medium

  * New upstream version
  * d/patches: Remove patch applied upstream.

 -- Lance Lin <lq27267@gmail.com>  Thu, 22 Aug 2024 21:31:59 +0700

toml11 (4.1.0-1) unstable; urgency=medium

  * New upstream version
  * d/rules: Build tests for all architectures.
  * d/patches: Add patch to manually cast the file size to std::streamsize
    for 32-bit architectures.
  * d/patches: Extend cmake-install.patch to generate arch-independent
    cmake config files.

 -- Lance Lin <lq27267@gmail.com>  Mon, 05 Aug 2024 22:35:37 +0700

toml11 (4.0.3-1) unstable; urgency=medium

  * New upstream version
  * d/patches: Refresh cmake-install.patch and
    include_doctest_from_external_dependency.patch.
  * d/patches: Add patch to downgrade uninitialized variable error
    to warning.
  * d/rules: Only build tests for amd64.

 -- Lance Lin <lq27267@gmail.com>  Thu, 18 Jul 2024 22:39:35 +0700

toml11 (4.0.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * d/patches: Refresh cmake-install.patch, remove allow_tests.patch,
    and add patch to use external header file in tests
  * d/tests/toml: Remove test files that are not needed
  * d/copyright: Update copyright for removed test files
  * d/control: Replace libbost-test-dev with nlohmann-json3-dev and
    doctest-dev in Build-Depends for testing framework
  * d/tests/control: Replace libbost-test-dev with nlohmann-json3-dev
    and doctest-dev in Depends for testing framework and remove git
  * d/rules: Update variable name to build tests, remove undefined option,
    and remove copy for test files that have been removed
  * d/tests/run-unit-test: Update variable name to build tests, remove
    undefined option, and remove git specific option

 -- Lance Lin <lq27267@gmail.com>  Fri, 28 Jun 2024 19:13:50 +0700

toml11 (3.8.1-2) unstable; urgency=medium

  * d/control: Add libboost-test-dev to Build-Depends for running tests
  * d/patches: Refresh and add patch to allow tests to run during build
  * d/tests/toml: Include test files from external source that were being
    cloned during testing
  * d/copyright: Add copyright for included test files in debian/tests/toml
  * d/rules: Additional options for running the tests during build and copy
    test files from debian/tests/toml directory before dh_auto_configure

 -- Lance Lin <lq27267@gmail.com>  Thu, 07 Mar 2024 19:32:29 +0700

toml11 (3.8.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * d/control: Update Uploaders email
  * d/rules: Set CMAKE_CXX_STANDARD for configuration
  * d/patches: Set Forwarded field for cmake-install.patch
  * d/tests/run-unit-test: Set CMAKE_CXX_STANDARD for configuration
  * d/copyright: Update copyright year and email for debian files

 -- Lance Lin <lq27267@gmail.com>  Fri, 12 Jan 2024 21:59:12 +0700

toml11 (3.7.1-3) unstable; urgency=medium

  [ Adrian Bunk ]
  * autopkgtest: Disable -Werror and run tests on all architectures

 -- Andreas Tille <tille@debian.org>  Fri, 13 May 2022 06:58:04 +0200

toml11 (3.7.1-2) unstable; urgency=medium

  * Team upload

  [ Adrian Bunk ]
  * Install the cmake files to the correct location, instead of moving the
    installed files

  [ Andreas Tille ]
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 12 May 2022 12:39:00 +0200

toml11 (3.7.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Skip autopkgtest for Architectures armhf and i386

 -- Andreas Tille <tille@debian.org>  Wed, 27 Apr 2022 10:15:52 +0200

toml11 (3.7.0-2) unstable; urgency=medium

  * Team upload.

  [ Lance Lin ]
  * Added autopkgtest files

  [ Andreas Tille ]
  * Source-only upload

 -- Andreas Tille <tille@debian.org>  Wed, 16 Feb 2022 12:16:46 +0100

toml11 (3.7.0-1) unstable; urgency=low

  * Initial release. Closes: #1004498

 -- Lance Lin <lqi254@protonmail.com>  Thu, 03 Feb 2022 20:22:26 +0700
